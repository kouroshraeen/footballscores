# Football Scores

Football Scores is part of the second student project for the Udacity Android Developer Nanodegree program.

This Application gets Football Matches Scores for the Premier League, Seria A, Bundesliga, Primera Division and the Uefa Champions League.
All Matches data are acquired from football-data.org free API. The original version of this app was created by Yahia Khalid Ibn El Walid Ahmed and was modified by Kourosh Raeen.

Note: You will need to provide your own football-data.org API key for the app to work. Open or create the gradle.properties file located in .gradle directory
on your local machine and add the following line: FootballDataAPIKey=YOUR_API_KEY.

You could also open the myFetchService.java file and change the line:

    m_connection.addRequestProperty("X-Auth-Token", BuildConfig.FOOTBALL_DATA_API_KEY);

to:

    m_connection.addRequestProperty("X-Auth-Token", YOUR_API_KEY);

where you replace YOUR_API_KEY with your own key.

## Additions

* Football Scores can be displayed in a collection widget
* Added content descriptions for all buttons
* Added support for RTL layout mirroring
* String resources are extracted into the strings.xml file
* Untranslatable strings have a translatable tag marked to false
