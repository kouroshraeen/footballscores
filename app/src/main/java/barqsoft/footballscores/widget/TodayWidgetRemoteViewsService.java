package barqsoft.footballscores.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import barqsoft.footballscores.DatabaseContract;
import barqsoft.footballscores.R;
import barqsoft.footballscores.Utilies;

/**
 * Created by Kourosh on 11/18/2015.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class TodayWidgetRemoteViewsService extends RemoteViewsService{

    private static final String LOG_TAG = TodayWidgetRemoteViewsService.class.getSimpleName();

    private static final String[] SCORE_COLUMNS = {
            DatabaseContract.SCORES_TABLE + "." + DatabaseContract.scores_table._ID,
            DatabaseContract.scores_table.HOME_COL,
            DatabaseContract.scores_table.HOME_GOALS_COL,
            DatabaseContract.scores_table.AWAY_COL,
            DatabaseContract.scores_table.AWAY_GOALS_COL,
            DatabaseContract.scores_table.TIME_COL,
            DatabaseContract.scores_table.DATE_COL
    };

    private static final int INDEX_SCORE_ID = 0;
    private static final int INDEX_HOME_TEAM = 1;
    private static final int INDEX_HOME_GOALS =2;
    private static final int INDEX_AWAY_TEAM = 3;
    private static final int INDEX_AWAY_GOALS = 4;
    private static final int INDEX_MATCH_TIME = 5;

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        Context context = this.getApplicationContext();
        return new ScoresWidgetRemoteViewsFactory(context, intent);
    }

    class ScoresWidgetRemoteViewsFactory implements RemoteViewsFactory {

        private Cursor data = null;
        private Context mContext;

        public ScoresWidgetRemoteViewsFactory(Context context, Intent intent) {
            mContext = context;
        }

        @Override
        public void onCreate() {

        }

        @Override
        public void onDataSetChanged() {
            if (data != null) {
                data.close();
            }

            Locale currentLocale = mContext.getResources().getConfiguration().locale;

            Uri todayScoresUri = DatabaseContract.scores_table.buildScoreWithDate();
            Calendar c = Calendar.getInstance();
            SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", currentLocale);
            data = getContentResolver().query(todayScoresUri,
                    SCORE_COLUMNS,
                    null,
                    new String[]{dateFormatter.format(c.getTime())},
                    DatabaseContract.scores_table.DATE_COL + " ASC");

            Log.d(LOG_TAG, "Cursor: " + data);

        }

        @Override
        public void onDestroy() {
            if (data != null) {
                data.close();
                data = null;
            }
        }

        @Override
        public int getCount() {
            return data.getCount();
        }

        @Override
        public RemoteViews getViewAt(int position) {
            if (position == AdapterView.INVALID_POSITION ||
                    data == null || !data.moveToPosition(position)) {
                Log.e(LOG_TAG, "cursor error");
                return null;
            }

            RemoteViews remoteViews = new RemoteViews(getPackageName(),
                    R.layout.widget_scores_list_item);

            Intent fillInIntent = new Intent();
            fillInIntent.putExtra("EXTRA_ITEM", position);
            remoteViews.setOnClickFillInIntent(R.id.widget_list_item, fillInIntent);

            String homeTeam = data.getString(INDEX_HOME_TEAM);
            Integer homeGoals = data.getInt(INDEX_HOME_GOALS);
            String awayTeam = data.getString(INDEX_AWAY_TEAM);
            Integer awayGoals = data.getInt(INDEX_AWAY_GOALS);
            String matchTime = data.getString(INDEX_MATCH_TIME);

            remoteViews.setTextViewText(R.id.widget_team_home, homeTeam);
            remoteViews.setTextViewText(R.id.widget_team_away, awayTeam);
            remoteViews.setTextViewText(R.id.widget_match_time, matchTime);

            if ((homeGoals > -1) && (awayGoals > -1)) {
                remoteViews.setTextViewText(R.id.widget_score, homeGoals.toString() + " - " + awayGoals.toString());
            }
            else {
                remoteViews.setTextViewText(R.id.widget_score, "-");
            }

            remoteViews.setImageViewResource(R.id.widget_icon_home ,Utilies.getTeamCrestByTeamName(homeTeam));
            remoteViews.setImageViewResource(R.id.widget_icon_away ,Utilies.getTeamCrestByTeamName(awayTeam));

            return remoteViews;
        }

        @Override
        public RemoteViews getLoadingView() {
            return null; // get the default loading view
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int i) {
            if (data.moveToPosition(i)){
                return data.getLong(INDEX_SCORE_ID);
            }
            return i;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }
    }
}
